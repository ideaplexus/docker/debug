# Debug Container

This is a very simple debug container based on alpine:latest which has a instance of supervisor running to ensure the container kept running.
You can get this container by running `docker pull ideaplexus/debug`.