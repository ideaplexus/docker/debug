ARG PROXY_CACHE_PREFIX

FROM ${PROXY_CACHE_PREFIX}alpine:3.21

ARG ID=99
ARG USER=debug
ARG HOME=/debug

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN apk add --no-cache --upgrade \
    bash=~5 \
    bash-completion=~2 \
    bind-tools=~9 \
    ca-certificates=~20241121 \
    coreutils=~9 \
    curl=~8 \
    jq=~1 \
    mariadb-client=~11 \
    nano=~8 \
    net-tools=~2 \
    nmap=~7 \
    openldap-clients=~2 \
    openssh=~9 \
    postgresql16-client=~16 \
    redis=~7 \
    supervisor=~4 \
    yaml=~0 \
    yq-go=~4 \
  && curl -sSL -O https://download.microsoft.com/download/7/6/d/76de322a-d860-4894-9945-f0cc5d6a45f8/msodbcsql18_18.4.1.1-1_amd64.apk \
  && curl -sSL -O curl -O https://download.microsoft.com/download/7/6/d/76de322a-d860-4894-9945-f0cc5d6a45f8/mssql-tools18_18.4.1.1-1_amd64.apk \
  && apk add --no-cache --allow-untrusted msodbcsql18_18.4.1.1-1_amd64.apk \
  && apk add --no-cache --allow-untrusted mssql-tools18_18.4.1.1-1_amd64.apk \
  && rm -f msodbcsql18_18.4.1.1-1_amd64.apk \
  && rm -f mssql-tools18_18.4.1.1-1_amd64.apk \
  && rm -rf /var/cache/apk/*

RUN set -eux \
	\
    && addgroup -g "$ID" -S "$USER" \
    && adduser -u "$ID" -G "$USER" -S -D -H -s /bin/bash -h "$HOME" "$USER"

COPY --chown=$ID:$ID supervisord.conf /usr/local/supervisor/supervisord.conf

CMD ["/usr/bin/supervisord", "-n", "-c", "/usr/local/supervisor/supervisord.conf"]

WORKDIR ${HOME}

USER ${USER}